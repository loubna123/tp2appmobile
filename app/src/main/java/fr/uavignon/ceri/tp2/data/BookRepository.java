package fr.uavignon.ceri.tp2.data;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import static fr.uavignon.ceri.tp2.data.BookRoomDatabase.databaseWriteExecutor;

public class BookRepository {

    private LiveData<List<Book>> allBooks;
    private MutableLiveData<List<Book>> selectedBook = new MutableLiveData<>();

    private BookDao boDao;

    public BookRepository(Application application) {
        BookRoomDatabase db = BookRoomDatabase.getDatabase(application);
        boDao = db.bookDao();
        allBooks = boDao.getAllBooks();
    }

    public void updateBook(Book book) {
        databaseWriteExecutor.execute(() -> {
            boDao.updateBook(book);
        });
    }


    public void insertBook(Book book){
        databaseWriteExecutor.execute(() -> {
            boDao.insertBook(book);
        });
    }


    public void getBook(long id){

        Future<List<Book>> fbook = (Future<List<Book>>) databaseWriteExecutor.submit(() -> {

            return boDao.getBook(id);
        });
        try {
            selectedBook.setValue(fbook.get());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }






    public void deleteBook(long id){
        databaseWriteExecutor.execute(() -> {
            boDao.deleteBook(id);
        });
    }






    public void deleteAllBooks(){
        databaseWriteExecutor.execute(() -> {
            boDao.deleteAllBooks();
        });
    }







    public LiveData<List<Book>> getAllBooks(){
        return allBooks;
    }






    public MutableLiveData<List<Book>> getSearchResults() {
        return selectedBook;
    }
}