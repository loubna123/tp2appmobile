package fr.uavignon.ceri.tp2.data;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

public class DetailViewModel extends AndroidViewModel
{
    private BookRepository repository;
    private LiveData<List<Book>> allBooks;
    private MutableLiveData<List<Book>> searchResults;

    public DetailViewModel (Application application) {
        super(application);
        repository = new BookRepository(application);
        allBooks = repository.getAllBooks();
        searchResults = repository.getSearchResults();
    }

    MutableLiveData<List<Book>> getSearchResults() {
        return searchResults;
    }
    LiveData<List<Book>> getAllProducts() {
        return allBooks;
    }
    public void insertProduct(Book book) {
        repository.insertBook(book);
    }

    public void deleteBook(long id) {
        repository.deleteBook(id);
    }
}
